<?xml version="1.0" encoding="UTF-8"?>
<tileset name="animatedHeart" tilewidth="32" tileheight="32">
 <image source="powerups/animatedHeart.png" width="128" height="32"/>
 <tile id="0">
  <properties>
   <property name="animation" value="true"/>
   <property name="anitype" value="animatedHearts"/>
   <property name="layer" value="powerups_life"/>
  </properties>
 </tile>
</tileset>
