<?xml version="1.0" encoding="UTF-8"?>
<tileset name="green_plant" tilewidth="24" tileheight="32">
 <image source="enemies/green_plant.png" width="48" height="32"/>
 <tile id="0">
  <properties>
   <property name="animation" value="true"/>
   <property name="anitype" value="greenPlant"/>
   <property name="layer" value="hostiles"/>
  </properties>
 </tile>
</tileset>
