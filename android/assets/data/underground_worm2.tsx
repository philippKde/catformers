<?xml version="1.0" encoding="UTF-8"?>
<tileset name="underground_worm2" tilewidth="26" tileheight="32">
 <image source="enemies/underground_worm2.png" width="208" height="32"/>
 <tile id="0">
  <properties>
   <property name="animation" value="true"/>
   <property name="anitype" value="worm2"/>
   <property name="layer" value="hostiles"/>
  </properties>
 </tile>
</tileset>
