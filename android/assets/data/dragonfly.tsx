<?xml version="1.0" encoding="UTF-8"?>
<tileset name="dragonfly" tilewidth="64" tileheight="50">
 <image source="scenery/dragonfly.png" width="256" height="50"/>
 <tile id="0">
  <properties>
   <property name="animation" value="true"/>
   <property name="anitype" value="dragonfly"/>
   <property name="layer" value="background"/>
  </properties>
 </tile>
</tileset>
