<?xml version="1.0" encoding="UTF-8"?>
<tileset name="greenGuy1" tilewidth="29" tileheight="32">
 <image source="enemies/greenGuy1.png" width="232" height="32"/>
 <tile id="0">
  <properties>
   <property name="animation" value="true"/>
   <property name="anitype" value="greenGuy1"/>
   <property name="layer" value="hostiles"/>
  </properties>
 </tile>
</tileset>
