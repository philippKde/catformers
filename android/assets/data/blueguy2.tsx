<?xml version="1.0" encoding="UTF-8"?>
<tileset name="blueguy2" tilewidth="24" tileheight="32">
 <image source="enemies/blueguy2.png" width="48" height="32"/>
 <tile id="0">
  <properties>
   <property name="animation" value="true"/>
   <property name="anitype" value="blueguy2"/>
   <property name="layer" value="hostiles"/>
  </properties>
 </tile>
</tileset>
