<?xml version="1.0" encoding="UTF-8"?>
<tileset name="greenguy3" tilewidth="27" tileheight="32">
 <image source="enemies/greenguy3.png" width="54" height="32"/>
 <tile id="0">
  <properties>
   <property name="animation" value="true"/>
   <property name="anitype" value="greenguy3"/>
   <property name="layer" value="hostiles"/>
  </properties>
 </tile>
</tileset>
