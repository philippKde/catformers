<?xml version="1.0" encoding="UTF-8"?>
<tileset name="underground_worm" tilewidth="29" tileheight="32" tilecount="8" columns="8">
 <image source="enemies/underground_worm.png" width="232" height="32"/>
 <tile id="0">
  <properties>
   <property name="animation" value="true"/>
   <property name="anitype" value="underground_worm"/>
   <property name="layer" value="hostiles"/>
  </properties>
 </tile>
</tileset>
