<?xml version="1.0" encoding="UTF-8"?>
<tileset name="greenguy2" tilewidth="32" tileheight="25">
 <image source="enemies/greenguy2.png" width="64" height="25"/>
 <tile id="0">
  <properties>
   <property name="animation" value="true"/>
   <property name="anitype" value="greenGuy2"/>
   <property name="layer" value="hostiles"/>
  </properties>
 </tile>
</tileset>
