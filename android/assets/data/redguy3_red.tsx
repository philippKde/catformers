<?xml version="1.0" encoding="UTF-8"?>
<tileset name="redguy3_red" tilewidth="29" tileheight="32">
 <image source="enemies/redguy3.png" width="58" height="32"/>
 <tile id="0">
  <properties>
   <property name="animation" value="true"/>
   <property name="anitype" value="redguy3_red"/>
   <property name="layer" value="hostiles"/>
  </properties>
 </tile>
</tileset>
