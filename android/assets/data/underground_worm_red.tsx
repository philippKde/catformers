<?xml version="1.0" encoding="UTF-8"?>
<tileset name="underground_worm_red" tilewidth="29" tileheight="32">
 <image source="enemies/underground_worm_red.png" width="232" height="32"/>
 <tile id="0">
  <properties>
   <property name="animation" value="true"/>
   <property name="anitype" value="underground_worm_red"/>
   <property name="layer" value="hostiles"/>
  </properties>
 </tile>
</tileset>
