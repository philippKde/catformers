package com.kiszka.catformers;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;

public class MyGestureProcessor implements GestureListener {
	
	private Player	player;
	
	public MyGestureProcessor(Player thePlayer)
	{
		player = thePlayer;
	}
	
	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		if (button == Input.Buttons.LEFT) {
			// Some stuff
			// Gdx.app.log("huhu", "touch down!");
			
			return false;
		}
		
		return false;
	}
	
	@Override
	public boolean tap(float x, float y, int count, int button) {
		// TODO Auto-generated method stub
		// Gdx.app.log("INFO", "TAP" + count);
		// player.fly();
		return false;
	}
	
	@Override
	public boolean longPress(float x, float y) {
		// TODO Auto-generated method stub
		
		return false;
	}
	
	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		// TODO Auto-generated method stub
		
		if (Math.abs(velocityX) > Math.abs(velocityY)) {
			if (velocityX > 250) {
				player.fly();
				// Gdx.app.log("INFO", "flingright " + velocityX);
			}
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		// TODO Auto-generated method stub
		
		return false;
	}
	
	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		
		return false;
	}
	
	@Override
	public boolean zoom(float initialDistance, float distance) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void pinchStop(){

	}
	
}
