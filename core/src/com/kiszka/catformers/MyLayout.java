package com.kiszka.catformers;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class MyLayout {
	
	final CatformersGame	myGame;
	
	public MyLayout(final CatformersGame theGame)
	{
		myGame = theGame;
	}
	
	public Table setupLayout()
	{
		// TextureAtlas atlas = new TextureAtlas();
		// Skin skin = new Skin(atlas);
		
		LabelStyle labelStyle = new LabelStyle();
		labelStyle.font = myGame.font;
		
		Texture countTexture = myGame.assets.get("data/star.png", Texture.class);
		Texture livesTexture = myGame.assets.get("data/heart.png", Texture.class);
		
		Image pimg = new Image(countTexture);
		Image limg = new Image(livesTexture);
		
		Label pointsLabel = new Label("", labelStyle);
		
		Label livesLabel = new Label("", labelStyle);
		livesLabel.setColor(Color.RED);
		
		Table table = new Table();
		table.setBounds((pimg.getWidth() + 10) - (myGame.screenWidth / 2),
				(myGame.screenHeight / 2) - (pimg.getHeight() + limg.getHeight() - 15), 0, 0);
		// table.debug();
		table.add(pimg).padRight(15);
		table.add(pointsLabel).padBottom(10);
		table.row();
		table.add(limg).padRight(15);
		table.add(livesLabel);
		
		return table;
	}
	
	public void updateLayout(Player myPlayer)
	{	
		
	}
}
