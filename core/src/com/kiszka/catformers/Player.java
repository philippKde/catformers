package com.kiszka.catformers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;

import javax.xml.soap.Text;

/**
 * Player is a game character, that has state and state time.
 */
public class Player {
	
	// enum states the player can hav e
	enum State
	{
		Standing, Walking, Jumping, Powerjumping
	}
	
	// Enum upgrades
	enum Upgrade
	{
		Normal, Live, Jump1, Jump2, Powerjump1, Powerjump2, Invuln1, Invuln2
	}
	
	private static final int	MAPLAYER_BACKGROUND	= 0;
	
	private static final int	MAPLAYER_WALLS		= 1;
	
	private static final int	MAPLAYER_POINTS		= 3;
	
	private static final int	MAPLAYER_GOAL		= 2;
	
	private static final int	MAPLAYER_LIVES		= 4;
	
	private static final int	MAPLAYER_POWERUPS	= 5;
	
	private static final int	MAPLAYER_HOSTILES	= 6;
	
	final CatformersGame				game;
	
	private SpriteBatch			batch;
	
	// textures
	private Texture				spielerTexture;
	
	// private Texture powerjumpTexture;
	
	private Texture				standTexture;
	
	private Texture				jumpTexture;
	
	private Animation			powerjump;
	
	private Animation			stand;
	
	private Animation			walk;
	
	private Animation			jump;
	
	private Sound				pling;
	
	// player attributed
	static float				WIDTH;
	
	static float				HEIGHT;
	
	// maximale laufgeschwindigkeit
	static float				MAX_VELOCITY		= 5f;
	
	float						CAP_VELOCITY_X		= 5f;
	
	// geschwindigkeit fuer einen sprung
	float						JUMP_VELOCITY		= 35f;
	
	// daempfung beim laufen ... so weit laeuft die figur weiter wenn einmal der bewege knopf gedrueckt wurde.
	static float				DAMPING				= 0.1f;
	
	// gravitation
	public float				GRAVITY				= -1.5f;
	
	private Pool<Rectangle>		rectPool			= new Pool<Rectangle>()
													{
														
														@Override
														protected Rectangle newObject()
														{
															return new Rectangle();
														}
													};
	
	private Array<Rectangle>	tiles				= new Array<Rectangle>();
	
	private Array<Rectangle>	goals				= new Array<Rectangle>();
	
	private Array<Rectangle>	powerups			= new Array<Rectangle>();
	
	// position and velocity
	final Vector2				position			= new Vector2();
	
	final Vector2				velocity			= new Vector2();
	
	// Set state
	State						state				= State.Walking;
	
	Upgrade						upgrade				= Upgrade.Normal;
	
	// state time
	float						stateTime			= 0;
	
	// statuses
	boolean						facesRight			= true;
	
	boolean						grounded			= false;
	
	// counters / goals
	public int					lives				= 3;
	
	public int					points				= 0;
	
	// current level
	public int					myLevel;
    // current level
    public int					myWorld;
	
	// reached the end of the level
	public boolean				reachedGoal			= false;
	
	// was hit by an enemy
	public boolean				wasHit				= false;
	
	private Helpers				helpers				= new Helpers();
	
	/**
	 * Constructor.
	 * 
	 * @param theGame
	 */
	public Player(final CatformersGame theGame)
	{
		game = theGame;
		myLevel = 1;
		myWorld = 1;
		
		reloadTextures();
	}
	
	/*
	 * detect horizontal collisions
	 * draws the tiles from layer 1 = walls and platform
	 */
	private void collisionDetectionHorizontal(TiledMap map)
	{
		// get a rectangle to represent the player and set it to the players position, and size
		Rectangle playerRectangle = rectPool.obtain();
		playerRectangle.set(position.x, position.y, Player.WIDTH, Player.HEIGHT);
		
		// Start and end coordinates for getting the tiles to check for collisions
		int startX, startY, endX, endY;
		
		// since we check horizontal collisions, startX and endX are identical
		
		if (velocity.x > 0)
		{
			startX = endX = (int) (position.x + Player.WIDTH + velocity.x);
			playerRectangle.x += velocity.x;
		}
		else
		{
			startX = endX = (int) (position.x + velocity.x);
		}
		
		// start y and endY represent the position at the start and end of the horizontal movement
		startY = (int) (position.y);
		endY = (int) (position.y + Player.HEIGHT);
		
		// load the tiles the player has moved accross from the map
		helpers.getTiles(map, startX, startY, endX, endY, tiles);
		
		// loop through the tiles
		for (Rectangle tile: tiles)
		{
			// check if the player rectangle overlaps/has hit a tile
			if (playerRectangle.overlaps(tile))
			{
				velocity.x = 0;
				state = Player.State.Standing;
				grounded = true;
				break;
			}
		}
		
		// reset the position of the player rectangle
		playerRectangle.x = position.x;
		rectPool.free(playerRectangle);
	}
	
	/*
	 * check vertical collisions
	 * draws the tiles from layer 1 = walls and platform
	 * 2 = goal
	 */
	private void collisionDetectionVertical(TiledMap map)
	{
		Rectangle playerRect = rectPool.obtain();
		playerRect.set(position.x, position.y, Player.WIDTH, Player.HEIGHT);
		int startX, startY, endX, endY;
		// if the Spieler is moving upwards, check the tiles to the top of it's
		// top bounding box edge, otherwise check the ones to the bottom
		if (velocity.y > 0)
		{
			startY = endY = (int) (position.y + Player.HEIGHT + velocity.y);
		}
		else
		{
			startY = endY = (int) (position.y + velocity.y);
		}
		
		// check for the players jumping upwards against walls
		startX = (int) (position.x);
		endX = (int) (position.x + Player.WIDTH);
		helpers.getTiles(map, startX, startY, endX, endY, tiles);
		playerRect.y += velocity.y;
		for (Rectangle tile: tiles)
		{
			if (playerRect.overlaps(tile))
			{
				// we actually reset the Spieler y-position here
				// so it is just below/above the tile we collided with
				// this removes bouncing :)
				if (velocity.y > 0)
				{
					
					position.y = tile.y - Player.HEIGHT;
					// we hit a block jumping upwards, let's destroy it!
					TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(MAPLAYER_WALLS);
					// remove the cell
					layer.setCell((int) tile.x, (int) tile.y, null);
					
				}
				else
				{
					position.y = tile.y + tile.height;
					// if we hit the ground, mark us as grounded so we can jump
					state = Player.State.Walking;
				}
				
				grounded = true;
				velocity.y = 0;
				break;
			}
		}
		rectPool.free(playerRect);
		

		// check if the user reached the level goal
		playerRect = rectPool.obtain();
		playerRect.set(position.x, position.y, Player.WIDTH, Player.HEIGHT);
		
		if (velocity.x > 0)
		{
			startX = endX = (int) (position.x + Player.WIDTH + velocity.x);
		}
		else
		{
			startX = endX = (int) (position.x + velocity.x);
		}
		startY = (int) (position.y);
		endY = (int) (position.y + Player.HEIGHT);
		helpers.getTiles(map, startX, startY, endX, endY, goals, MAPLAYER_GOAL);
		
		playerRect.x += velocity.x;
		for (Rectangle goalTile: goals)
		{
			if (playerRect.overlaps(goalTile))
			{
				velocity.x = 0;
				reachedGoal = true;
				myLevel += 1;
				game.prefs.putInteger("playerLevel", myLevel);
				game.prefs.flush();
				
				break;
			}
		}
		playerRect.x = position.x;
		rectPool.free(playerRect);
	}
	
	/*
	 * check for hostile
	 * draws the tiles from layer 6 = hostiles
	 */
	private void hostilesDetect(TiledMap map)
	{
		Rectangle playerRectangle = rectPool.obtain();
		playerRectangle.set(position.x, position.y, Player.WIDTH, Player.HEIGHT);
		int startX, startY, endX, endY;
		
		startX = endX = (int) (position.x + Player.WIDTH + velocity.x);
		// properly catch powerups behind the player
		startX = startX - 1;
		
		startY = (int) (position.y);
		endY = (int) (position.y + Player.HEIGHT);
		
		helpers.getTiles(map, startX, startY, endX, endY, powerups, MAPLAYER_HOSTILES);
		playerRectangle.x += velocity.x;
		
		for (Rectangle tile: powerups)
		{
			if (playerRectangle.overlaps(tile))
			{
				Sound pling = game.assets.get("sound/coin.wav", Sound.class);
				pling.play(1.0f);
				lives -= 1;
				wasHit = true;
				break;
			}
		}
		playerRectangle.x = position.x;
		rectPool.free(playerRectangle);
	}
	
	/*
	 * collision detection for collectibe items like coins
	 * draws the tiles from layer
	 * 3 = points
	 * 4 = lives
	 * 5 = jump powerups
	 */
	private void pointsDetect(TiledMap map)
	{
		Rectangle playerRect = rectPool.obtain();
		playerRect.set(position.x, position.y, Player.WIDTH, Player.HEIGHT);
		int startX, startY, endX, endY;
		
		startX = endX = (int) (position.x + Player.WIDTH + velocity.x);
		// properly catch powerups behind the player
		startX = startX - 2;
		
		startY = (int) (position.y);
		endY = (int) (position.y + Player.HEIGHT);
		
		helpers.getTiles(map, startX, startY, endX, endY, powerups, MAPLAYER_POINTS);
		playerRect.x += velocity.x;
		
		// detect points
		for (Rectangle tile: powerups)
		{
			if (playerRect.overlaps(tile))
			{
				TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(MAPLAYER_POINTS);
				layer.setCell((int) tile.x, (int) tile.y, null);
				pling = game.assets.get("sound/coin.wav", Sound.class);
				pling.play(1.0f);
				points += 1;
				break;
			}
		}
		
		// detect live powerups
		helpers.getTiles(map, startX, startY, endX, endY, powerups, MAPLAYER_LIVES);
		
		for (Rectangle tile: powerups)
		{
			if (playerRect.overlaps(tile))
			{
				TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(MAPLAYER_LIVES);
				layer.setCell((int) tile.x, (int) tile.y, null);
				pling = game.assets.get("sound/health.wav", Sound.class);
				pling.play(1.5f);
				lives += 1;
				
				break;
			}
		}
		
		// detect jump powerups
		helpers.getTiles(map, startX, startY, endX, endY, powerups, MAPLAYER_POWERUPS);
		
		// this.upgrade = Spieler.Upgrade.Normal;
		for (Rectangle tile: powerups)
		{
			if (playerRect.overlaps(tile))
			{
				// get the map layer the powerup is on
				TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(MAPLAYER_POWERUPS);
				// remove the powerup
				layer.setCell((int) tile.x, (int) tile.y, null);
				// load and play a sound
				pling = game.assets.get("sound/teleport.wav", Sound.class);
				pling.play(1.3f);
				// set the upgrade status of the player to reflect the superjump
				upgrade = Player.Upgrade.Jump1;
				
				break;
			}
		}
		rectPool.free(playerRect);
		
	}
	
	public void dispose()
	{
		batch.dispose();
		game.assets.dispose();
		
	}
	
	public State getState()
	{
		return state;
	}
	
	public Upgrade getUpgrade()
	
	{
		return upgrade;
	}
	
	public void run()
	{
		GRAVITY = -2.0f;
		velocity.x = Player.MAX_VELOCITY;
		
		if (state != Player.State.Standing)
		{
			state = Player.State.Walking;
		}
		
		grounded = true;
		facesRight = true;
	}
	
	public void jump()
	{
		GRAVITY = -1.9f;
		// detect jump upgrade available
		float myVelocity;
		if (upgrade == Player.Upgrade.Jump1)
		{
			myVelocity = JUMP_VELOCITY * 1.3f;
			upgrade = Player.Upgrade.Normal;
		}
		else
		{
			myVelocity = JUMP_VELOCITY;
		}
		if (velocity.y == 0.0)
		{
			velocity.y += myVelocity;
			velocity.x *= 1.2;
		}
		state = Player.State.Jumping;
		grounded = false;
	}
	
	public void fly()
	{
		if (state != Player.State.Powerjumping)
		{
			CAP_VELOCITY_X = 101.0f;
			GRAVITY = -1.0f;
			
			position.y += 2;
			position.x -= 2;
			
			velocity.x = 100.0f;
			state = Player.State.Powerjumping;
			grounded = false;
			pling = game.assets.get("sound/teleport.wav", Sound.class);
			pling.play(1.0f);
			GRAVITY = -2.0f;
		}
		
	}
	
	public void reloadTextures()
	{
		// load the Spieler frames, split them, and assign them to Animations
		
		// base texture
		spielerTexture = game.assets.get("data/kuerbisrun.png", Texture.class);
		// spielerTexture = game.assets.get("data/sylvanas2_walk.png",Texture.class);
		
		TextureRegion[] regions = TextureRegion.split(spielerTexture, 50, 64)[0];
		// figure out the width and height of the Spieler for collision
		// detection and rendering by converting the player tiles into world units (1 unit = 16pixel)
		// Player.WIDTH = 1 / 16f * regions[0].getRegionWidth();
		// Player.HEIGHT = 1 / 16f * regions[0].getRegionHeight();
		
		Player.WIDTH = 1 / 32f * regions[0].getRegionWidth();
		Player.HEIGHT = 1 / 32f * regions[0].getRegionHeight();
		
		// Standing / Idle texture
		standTexture = game.assets.get("data/kuerbisidle.png", Texture.class);
		// TextureRegion[] standRegions = TextureRegion.split(standTexture, 40, 39)[0];
		TextureRegion[] standRegions = TextureRegion.split(standTexture, 50, 64)[0];
		
		// standing / idle animation
		stand =
				new Animation(0.15f, standRegions[0], standRegions[1], standRegions[2], standRegions[3],
						standRegions[4], standRegions[5], standRegions[6], standRegions[7]);
		stand.setPlayMode(Animation.PlayMode.NORMAL);
		
		// Walking animation
		// laufen ist eine animation aus mehreren frames
		walk =
				new Animation(0.12f, regions[0], regions[1], regions[2], regions[3], regions[4], regions[5],
						regions[6], regions[7]);
		walk.setPlayMode(Animation.PlayMode.LOOP);

		// Jumping animation

		
		// jumpTexture = game.assets.get("data/cat6_jump_red.png",Texture.class);
		jumpTexture = game.assets.get("data/kuerbisjump.png", Texture.class);
		
		// TextureRegion[] jumpRegions = TextureRegion.split(jumpTexture, 32, 44)[0];
		// TextureRegion[] jumpRegions = TextureRegion.split(jumpTexture, 27, 29)[0];
		TextureRegion[] jumpRegions = TextureRegion.split(jumpTexture, 50, 64)[0];
		
		jump = new Animation(0.2f, jumpRegions[0], jumpRegions[1], jumpRegions[2], jumpRegions[3], jumpRegions[4],
						jumpRegions[5], jumpRegions[6], jumpRegions[7]);
		jump.setPlayMode(Animation.PlayMode.NORMAL);
		
		// powerjump animation
		powerjump = jump;
		
		// powerjumpTexture = game.assets.get("data/cat_powerjump2.png", Texture.class);
		// TextureRegion[] powerjumpRegions = TextureRegion.split(powerjumpTexture, 62, 52)[0];
		// new Animation(0.2f,powerjumpRegions[0],powerjumpRegions[1]);
		// powerjump.setPlayMode(Animation.LOOP_PINGPONG);
		
	}
	
	public void renderPlayer(OrthogonalTiledMapRenderer renderer, float deltaTime)
	{
		// based on the Spieler state, get the animation frame
		TextureRegion frame = null;
		
		// Gdx.app.log("huhu", this.state.toString());
		switch (state)
		{
			case Jumping:
				frame = (TextureRegion) jump.getKeyFrame(stateTime);
				break;
			case Powerjumping:
				frame = (TextureRegion) powerjump.getKeyFrame(stateTime);
				break;
			case Standing:
				frame = (TextureRegion) stand.getKeyFrame(stateTime);
				run();
				break;
			case Walking:
				frame = (TextureRegion) walk.getKeyFrame(stateTime);
				run();
				break;
		
		}
		
		batch = (SpriteBatch) renderer.getBatch();

		batch.setProjectionMatrix(game.camera.calculateParallaxMatrix(1, 1));
		batch.begin();
		batch.draw(frame, position.x, position.y, Player.WIDTH, Player.HEIGHT);
		batch.end();
		
	}
	
	public void updatePlayer(TiledMap map, float deltaTime)
	{
		// wenn keine zeit vergangen ist, nix tun
		if (deltaTime == 0)
		{
			return;
		}
		stateTime += deltaTime;
		
		// clamp the velocity to the maximum, x-axis only
		if (Math.abs(velocity.x) > CAP_VELOCITY_X)
		{
			if (state != State.Jumping)
			{
				velocity.x = Math.signum(velocity.x) * Player.MAX_VELOCITY;
				CAP_VELOCITY_X = 10f;
				GRAVITY = -2.0f;
			}
		}
		// apply gravity
		velocity.add(0, GRAVITY);
		
		// limit the velocity to 0 if it's < 1, and set the state to standign
		if (Math.abs(velocity.x) < 1)
		{
			velocity.x = 0;
			state = Player.State.Standing;
		}
		
		// limit the position to the top of the map
		if (position.y > 30f)
		{
			
			position.y = 30;
		}
		// if y velocity is > 1 , player is jumping
		if (velocity.y > 1)
		{
			state = Player.State.Jumping;
			
		}
		
		// if x velocity is higher than max running speed - user is powerjumping
		if (Math.abs(velocity.x) > (Player.MAX_VELOCITY * 9))
		{
			state = Player.State.Powerjumping;
		}
		
		// multiply by delta time so we know how far we go
		// in this frame
		velocity.scl(deltaTime);
		
		// perform collision detection & response, on each axis, separately
		// check points
		pointsDetect(map);
		
		// check collision on the y axis (when jumping)
		collisionDetectionVertical(map);
		
		if (state != Player.State.Powerjumping)
		{
			// check collision on the x axis.
			collisionDetectionHorizontal(map);
			// check for collisions with hostiles
			hostilesDetect(map);
		}
		
		// unscale the velocity by the inverse delta time and set
		// the latest position
		position.add(velocity);
		// this.velocity.mul(1 / deltaTime);
		velocity.scl(1 / deltaTime);
		
		// Apply damping to the velocity on the x-axis so we don't
		// walk infinitely once a key was pressed
		// this.velocity.x *= Spieler.DAMPING;
	}
}
