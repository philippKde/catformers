package com.kiszka.catformers;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.maps.tiled.TiledMapTileSets;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;


public class MainScreen implements Screen {

    private static final String HEART_PNG = "data/heart.png";

    private static final String STAR_PNG = "data/star.png";

    private static final float MUSIC_VOLUME = 0.3f;

    private static final float WORLD_UNITSIZE = 32f;

    private static final int PLAYER_STARTX = 32;
    private static final int PLAYER_STARTY = 1;

    final CatformersGame game;

    private Player thePlayer;

    private Stage stage;

    private Label pointsLabel;

    private Label livesLabel;

    private Label levelName;

    private Table table;

    private String mapname;

    private Texture parallaxTexture2;

    private Texture parallaxTexture3;

    private Texture countTexture;

    private Texture livesTexture;

    private Image pointsImage;

    private Image livesImage;

    private Texture bgTexture;

    private Texture parallaxTexture = null;

    /**
     * Background images
     */
    private TextureRegion[] bgLayers = new TextureRegion[3];

    public Skin skin;

    public String theLevel;

    public TiledMap map;

    private TmxMapLoader maploader = new TmxMapLoader();

    private OrthogonalTiledMapRenderer renderer;

    private Music music;

    private String playerLevel;

    /**
     * Constructor.
     *
     * @param game
     */
    public MainScreen(final CatformersGame game) {
        this.game = game;

        initStage();
        initInputProcessor();
        initLevel();
    }

    /*
     * Load the current player level from the config
     * then call start game
     *
     */
    private void initLevel() {

        // load the map, set the unit scale to 1/16 (1 unit == 16 pixels)
        int level = game.prefs.getInteger("playerLevel");
        int world = game.prefs.getInteger("playerWorld");


        if (level > 0) {
            thePlayer.myLevel = level;
        } else {
            thePlayer.myLevel = 1;
        }
        if (world > 0) {
            thePlayer.myWorld = world;
        } else {
            thePlayer.myWorld = 1;
        }

        this.playerLevel = thePlayer.myWorld + "-" + thePlayer.myLevel;

        String levelFileName = "data/level" + this.playerLevel + ".tmx";
        // check if the level file exists
        // if no default to level 1
        boolean levelFileExists = Gdx.files.internal(levelFileName).exists();


        Gdx.app.log("level file", levelFileName + " exists: " + levelFileExists);
        if (!levelFileExists) {
            thePlayer.myLevel = 1;
            thePlayer.myWorld = 1;
            this.playerLevel = "1-1";
        }

        Gdx.app.log("player level", this.playerLevel);


        stage.getViewport().update(game.screenWidth, game.screenHeight, true);
        game.viewport.update(game.screenWidth, game.screenHeight);

        this.map = startGame(this.playerLevel);
    }

    /*
     *
     * Start a new game
     */
    private TiledMap startGame(String levelname) {
        theLevel = levelname;
        // remove to activate level progression
        //levelname = "level1-3.tmx";
        String gameLevel = "level" + levelname + ".tmx";


        map = maploader.load("data/" + gameLevel);

        // initialise music
        loadMusic(map);

        // replace static images with animations
        animateTiles(map);

        // setup the camera to constrain the viewport to the map height
        setupCamera(map);

        // get the level title
        mapname = map.getProperties().get("name", String.class);

        levelName = new Label(mapname, game.labelStyleLarge);
        levelName.setBounds((game.screenWidth / 2) - levelName.getWidth() / 2, (game.screenHeight / 2), 0, 0);
        // levelName.setFontScale(1.2f);
        levelName.addAction(Actions.sequence(Actions.fadeOut(5f), Actions.removeActor()));

        stage.addActor(levelName);

        this.renderer = new OrthogonalTiledMapRenderer(map, 1 / WORLD_UNITSIZE);
        game.camera.update();

        return map;
    }

    /*
     *
     * Start a discrete level within an existing game
     */
    private void startLevel(String theLevel) {

        // clear any loaded maps
        map.dispose();
        map = maploader.load("data/level" + theLevel + ".tmx");

        // replace static images with animations
        animateTiles(map);

        // setup the camera to constrain the viewport to the map height
        setupCamera(map);

        // get mapname
        mapname = map.getProperties().get("name", String.class);

        // add map name label
        levelName = new Label(mapname, game.labelStyleLarge);
        levelName.setBounds((game.screenWidth / 2) - levelName.getWidth() / 2, (game.screenHeight / 2), 0, 0);
        levelName.addAction(Actions.fadeOut(5f));

        stage.addActor(levelName);


        // reset player on new level
        thePlayer.reachedGoal = false;
        thePlayer.position.set(PLAYER_STARTX, PLAYER_STARTY);
        thePlayer.velocity.x = Player.MAX_VELOCITY;
        //thePlayer.velocity.y = 1f;
        thePlayer.points = 0;
        //thePlayer.velocity.add(0, thePlayer.GRAVITY);

        thePlayer.state = Player.State.Walking;
        thePlayer.upgrade = Player.Upgrade.Normal;
        renderer.setMap(map);
    }

    private void initStage() {
        stage = new Stage();

        // create the Player we want to move around the world
        thePlayer = new Player(game);
        thePlayer.position.set(PLAYER_STARTX, PLAYER_STARTY);
        thePlayer.velocity.x = Player.MAX_VELOCITY;

        countTexture = game.assets.get(STAR_PNG, Texture.class);
        livesTexture = game.assets.get(HEART_PNG, Texture.class);

        pointsImage = new Image(countTexture);
        livesImage = new Image(livesTexture);

        pointsLabel = new Label("" + String.valueOf(thePlayer.points), game.labelStyle);

        livesLabel = new Label(String.valueOf(thePlayer.lives), game.labelStyle);
        livesLabel.setColor(Color.RED);

        initTable();

        stage.addActor(table);
    }

    private void initTable() {
        table = new Table();
        table.setFillParent(true);

        // set bounds at half screen
        table.setBounds((pointsImage.getWidth() + pointsLabel.getWidth()) - (game.screenWidth / 2), (game.screenHeight / 2)
                - (pointsLabel.getHeight() + livesLabel.getHeight() + 10) / 2, 0, 0);
        // table.debug();
        table.add(pointsImage).padRight(15).padBottom(5);
        table.add(pointsLabel);
        table.row();
        table.add(livesImage).padRight(15);
        table.add(livesLabel);
    }

    private void initInputProcessor() {
        // create input multiplexer for handling multiple input sources
        InputMultiplexer im = new InputMultiplexer();
        // create input processor1 - handling basic touch, mouse and keyboard interaction
        MyInputProcessor inputProcessor1 = new MyInputProcessor(thePlayer);

        // create gesture listener and add it to gesture detector
        // this handles multitouch and gestures
        MyGestureProcessor gestureListener = new MyGestureProcessor(thePlayer);
        GestureDetector gestureProcessor = new GestureDetector(gestureListener);

        // add to multiplexer
        im.addProcessor(gestureProcessor);
        im.addProcessor(inputProcessor1);
        // tell libgdx to route input through the multiplexer
        Gdx.input.setInputProcessor(im);
    }


    private void gameOver() {
        if (music != null) {
            Gdx.app.log("music", music.toString());
            music.stop();
        }

        // dispose the map
        map.dispose();
        game.camera.update();
        thePlayer.wasHit = false;
        // reset the level to the first one
        thePlayer.myLevel = 1;

        game.prefs.putInteger("playerLevel", 1);
        game.prefs.flush();
        // show the splash screen
        game.setScreen(new SplashScreen(game, MyStage.GAMEOVER));
        hide();
    }


    @Override
    public void render(float delta) {
        // called every frame
        clearScreenGL();

        // get the delta time
        float deltaTime = Gdx.graphics.getDeltaTime();

        // load background image from the TMX map
        loadBackground(map);

        // process player interaction
        processPlayer(deltaTime);

        // update the camera to follow the player, constrain the viewport etc
        updateCamera(map);

        // set the tile map rendere view based on what the
        // camera sees and render the map
        renderer.setView(game.camera);
        renderer.render();

        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / WORLD_UNITSIZE));
        stage.draw();

        // render the player
        if (thePlayer.lives > 0) {
            thePlayer.renderPlayer(renderer, deltaTime);
        }
    }

    private void clearScreenGL() {
        //Gdx.graphics.getGL20().glClearColor(0.8f, 0.6f, 1.0f, 1);
        // Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0));
    }

    private void processPlayer(float deltaTime) {
        // player fell down
        if (thePlayer.position.y < 1) {
            thePlayer.lives -= 1; // remove a life

            // if no lives are left, end the game, otherwise start over
            if (thePlayer.lives < 1) {
                thePlayer.lives = 0;

                gameOver();
            } else {

                startLevel(this.playerLevel);
            }

        }

        // no lives left - game over
        if (thePlayer.lives < 1) {
            thePlayer.lives = 0;
            gameOver();
        }

        // player completed the level - start next level
        if (thePlayer.reachedGoal) {
            if (music != null) {
                Gdx.app.log("music", music.toString());
                music.stop();
            }

            game.setScreen(new SplashScreen(game, MyStage.LEVELOVER));
            return;
        }

        // the player was hit by a hostile - restart level
        if (thePlayer.wasHit) {

            thePlayer.wasHit = false;
            startLevel(this.playerLevel);
            return;
        }

        // update the player (process input, collision detection, position update)
        thePlayer.updatePlayer(map, deltaTime);

        // update the displays for points and lives
        pointsLabel.setText(String.valueOf(thePlayer.points));
        livesLabel.setText(String.valueOf(thePlayer.lives));
    }

    private void updateCamera(TiledMap map) {
        // let the camera follow the player, x-axis only

        game.camera.position.x = thePlayer.position.x;

        if (game.camera.position.y < 0) {
            game.camera.position.y = 1;
        }

        game.camera.update();


    }

    @Override
    public void resize(int width, int height) {

        stage.getViewport().update(width, height, true);
        game.viewport.update(width, height);
        game.viewport.apply(true);
        table.invalidateHierarchy();
        table.setSize(width, height);
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        thePlayer.dispose();
        map.dispose();
        renderer.dispose();
        bgTexture.dispose();

        stage.dispose();
    }

    private void setupCamera(TiledMap map) {
        TiledMapTileLayer mainLayer = (TiledMapTileLayer) map.getLayers().get(0);
        // int tileSize = (int) mainLayer.getTileWidth();
        // int mapWidth = mainLayer.getWidth() * tileSize;
        // int mapHeight = mainLayer.getHeight() * tileSize;
        game.camera.setToOrtho(false, CatformersGame.VIEWPORT_WIDTH, mainLayer.getHeight());
        game.camera.update();
    }

    private void loadMusic(TiledMap theMap) {

        String musicProperty = theMap.getProperties().get("music", String.class);
        String musicName = "sound/music/" + musicProperty + ".mp3";

        music = game.assets.get(musicName, Music.class);

        music.setVolume(MUSIC_VOLUME);
        music.setLooping(true);
        music.play();

    }

    private void loadBackground(TiledMap theMap) {
        // get all tilesets
        TiledMapTileSets tileSets = theMap.getTileSets();
        Iterator<TiledMapTileSet> tileSetsIterator = tileSets.iterator();

        while (tileSetsIterator.hasNext()) {
            // get the tile set , and an iterator for the tiles in it
            TiledMapTileSet tileSet = tileSetsIterator.next();
            // Gdx.app.log("tileset", tileSet.getName());

            if (tileSet.getName().equals("background")) {
                Iterator<TiledMapTile> tiles = tileSet.iterator();
                TiledMapTile tile = tiles.next();
                bgTexture = tile.getTextureRegion().getTexture();
            }

            if (tileSet.getName().equals("background_near")) {
                MapProperties tileProperties = tileSet.getProperties();

                Iterator<TiledMapTile> tiles = tileSet.iterator();
                TiledMapTile tile = tiles.next();

                parallaxTexture = tile.getTextureRegion().getTexture();
            }

        }

        if (bgTexture != null) {
            /* code for rendering a fixed BG */
            bgTexture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
            // create a texture region from the texture. this is a cutour of the texture.
            TextureRegion region = new TextureRegion(bgTexture, 0, 0, bgTexture.getWidth(), bgTexture.getHeight());
            bgLayers[0] = region;
            // create a sprite
            Sprite sprite = new Sprite(region);
            // get a sprite batch for rendering
            SpriteBatch batch = (SpriteBatch) renderer.getBatch();
            // set the projection matrix to the fixed camera for the background. so it doesnt move.
            batch.setProjectionMatrix(game.fixedCamera.combined);
            // improve performance
            batch.disableBlending();
            // render the background
            batch.begin();
            batch.draw(sprite, 0, 0, 32, 32);
            batch.end();
            batch.enableBlending();

            if (parallaxTexture != null) {
                // set the camera to the moving one again
                 batch.setProjectionMatrix(game.camera.combined);
                /* code for rendering a scrolling bg */
                Texture parallaxTexture = parallaxTexture2 = parallaxTexture3 = this.parallaxTexture;

                bgTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

                SpriteBatch paraBatch = (SpriteBatch) renderer.getBatch();
                // debug mode performance
                // paraBatch.disableBlending();


                int parallaxTextureWidth = parallaxTexture.getWidth();
                int parallaxTextureHeight = parallaxTexture.getHeight();
                // get world units width of texture
                int parallaxTextureUnitsWidth = parallaxTextureWidth / 32;
                int parallaxTextureUnitsHeight = parallaxTextureHeight / 32;



                float parallaxProjectionX = 1f;
                float parallaxProjectionY = 1f;

                paraBatch.setProjectionMatrix(game.camera.calculateParallaxMatrix(parallaxProjectionX, parallaxProjectionY));
                paraBatch.begin();

                // get effective player position
                int effPos = (int) (thePlayer.position.x + Player.WIDTH);
                // divide into 5 sectors


                // count how often the tile has been passed
                int playerPassedParallaxTextureCount = effPos / parallaxTextureUnitsWidth;
                // convert to x position
                int tileOffset = playerPassedParallaxTextureCount * parallaxTextureUnitsWidth;

                // draw the texture 3 times
                paraBatch.draw(parallaxTexture, tileOffset - parallaxTextureUnitsWidth, 0, parallaxTextureUnitsWidth, parallaxTextureUnitsHeight);
                paraBatch.draw(parallaxTexture2, tileOffset, 0, parallaxTextureUnitsWidth, parallaxTextureUnitsHeight);
                paraBatch.draw(parallaxTexture3, tileOffset + parallaxTextureUnitsWidth, 0, parallaxTextureUnitsWidth, parallaxTextureUnitsHeight);

                paraBatch.end();
            }
        }
    }

    /*
     * This reads the declarative meta information added to tiles in tiled
     * animation: true = animated
     * anitype: arbitrary name
     * layer: map layer the animation belongs to (hostiles, powerups, etc)
     */
    private void animateTiles(TiledMap theMap) {
        // get all tilesets
        TiledMapTileSets tileSets = theMap.getTileSets();
        Iterator<TiledMapTileSet> tileSetsIterator = tileSets.iterator();

        // array for storing tileset and layer names
        // tileset names determine the tilesets to make animations from
        // layer names determine the layer to put the animation on
        Array<String> tileSetNames = new Array<String>();
        Array<String> layerNames = new Array<String>();

        // Scan the tileset for static tiles marked as animations
        // iterate over tilesets
        while (tileSetsIterator.hasNext()) {

            // get the tile set , and an iterator for the tiles in it
            TiledMapTileSet tileSet = tileSetsIterator.next();
            Iterator<TiledMapTile> tiles = tileSet.iterator();

            // iterate over tiles in the tileset
            if (tiles.hasNext()) {
                TiledMapTile tile = tiles.next();

                // if the tile has the animation key add it to the list of animatable tiles
                if (tile.getProperties().containsKey("animation")
                        && tile.getProperties().get("animation", String.class).equals("true")) {

                    // add name of the tileset to be animated and name of the layer to place the animation
                    layerNames.add(tile.getProperties().get("layer", String.class));
                    tileSetNames.add(tileSet.getName());
                }
            }
        }

        int i = 0; // counter for iterations

        // create the animations
        // iterate over the tilesets that have static tiles to be animated
        for (String tileSetName : tileSetNames) {

            // load the tileset
            TiledMapTileSet theTiles = theMap.getTileSets().getTileSet(tileSetName);

            // get an iterator
            Iterator<TiledMapTile> tilesIterator = theTiles.iterator();

            // create a new array to store static tiles
            Array<StaticTiledMapTile> staticTileList = new Array<StaticTiledMapTile>(theTiles.size());

            // iterate over the static tiles and add them as static tiles to the array
            while (tilesIterator.hasNext()) {
                TiledMapTile staticTile = tilesIterator.next(); // get the tile

                if (staticTile != null) {
                    // add the static tile to the tile array
                    staticTileList.add((StaticTiledMapTile) staticTile);
                }
            }

            // create an animated tile from the static tiles written to the array before
            // set animations peed here
            AnimatedTiledMapTile animatedTile = new AnimatedTiledMapTile(0.20f, staticTileList);

            // re set the properties of the animated tile to those of the static tiles
            for (TiledMapTile tile : staticTileList) {
                animatedTile.getProperties().putAll(tile.getProperties());
            }

            // tile type of the current tile
            String tileType = animatedTile.getProperties().get("anitype", String.class);

            // get the map layer the animation should be put on - use the layer name corresponding
            // to the current animatedTile
            TiledMapTileLayer mapLayer = (TiledMapTileLayer) theMap.getLayers().get(layerNames.get(i));
            // iterate over the grid of the layer
            for (int x = 0; x < mapLayer.getWidth(); x++) {
                for (int y = 0; y < mapLayer.getHeight(); y++) {

                    // get the cell in the grid we are at
                    Cell cell = mapLayer.getCell(x, y);

                    // if the cell is not empty, a tile was placed on it in the editor
                    if (cell != null) {
                        // Read cell type of the current grid cell
                        if (null != cell.getTile().getProperties()) {
                            String cellType = cell.getTile().getProperties().get("anitype", String.class);

                            // cell type is the type of the cell as set in the editor - can be empty or the name of a
                            // tile
                            // to be animated
                            // tile type contains the type of the current tile
                            // if the cell type from the editor corresponds to the current animation, set the cell
                            // content
                            // to the animated tile
                            if (null != cellType && cellType.equals(tileType)) {
                                cell.setTile(animatedTile);
                            }
                        }
                    }
                }
            }

            i = i + 1;
        }
    }
}
