package com.kiszka.catformers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kiszka.catformers.gui.MyButton;

/**
 * SplashScreen is a ...
 * 
 * @author philipp
 * @author Eva.Kiszka
 */
public class SplashScreen implements Screen {
	
	private static final int	Y_0				= 0;
	
	private static final int	X_0				= 0;
	
	private static final String	GAMEOVER_PNG	= "data/background/BG_forest3.png";
	
	private static final String	SPLASH_PNG		= "data/background/BG_forest3.png";
	
	final private CatformersGame		game;
	
	private Texture				logo;
	
	private SpriteBatch			batch			= new SpriteBatch();
	
	private Stage				stage;
	
	/**
	 * Constructor.
	 * 
	 * @param game
	 * @param stage
	 */
	public SplashScreen(final CatformersGame game, final MyStage stage) {
		this.game = game;
		
		switch (stage) {
			case START:
				logo = new Texture(Gdx.files.internal(SPLASH_PNG));
				initGui("Start");
				break;
			case GAMEOVER:
				logo = game.assets.get(GAMEOVER_PNG, Texture.class);
				initGui("Try again");
				break;
			case LEVELOVER:
				logo = game.assets.get(GAMEOVER_PNG, Texture.class);
				initGui("Next level");
				break;
			default:
				Gdx.app.error("", "Don't know how to handle stage '" + stage.toString() + "'");
				break;
		}
	}
	
	private void initGui(String buttontext)
	{
		
		// create the stage .this is the root element of the scene2d scene graph
		stage = new Stage();
		
		// add the table to the stage
		MyButton startButton = new MyButton(game);
		
		stage.addActor(startButton.getButton(buttontext, new ClickListener() {
			
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(new MainScreen(game));
			}
		})
				);
		Gdx.input.setInputProcessor(stage);
	}
	
	public void clearStage()
	{
		stage.clear();
	}
	
	@Override
	public void render(float delta) {
		if (Gdx.input.justTouched()) {
			// game.setScreen(new MainScreen(game));
		}
		
		// Gdx.graphics.getGL20().glClearColor( 0.7f, 0.7f, 1.0f, 1);
		// Gdx.graphics.getGL20().glClear( GL20.GL_COLOR_BUFFER_BIT );
		
		batch.setProjectionMatrix(game.fixedCamera.combined);
		batch.begin();
		batch.draw(logo, X_0, Y_0, CatformersGame.VIEWPORT_WIDTH, CatformersGame.VIEWPORT_HEIGHT);
		batch.setProjectionMatrix(game.camera.combined);
		batch.end();
		float deltaTime = Gdx.graphics.getDeltaTime();
		stage.act(deltaTime);
		
		stage.draw();
	}
	
	@Override
	public void dispose() {
		batch.dispose();
		logo.dispose();
		stage.dispose();
	}
	
	@Override
	public void resize(int width, int height) {}
	
	@Override
	public void show() {}
	
	@Override
	public void hide() {
		stage.clear();
		
	}
	
	@Override
	public void pause() {}
	
	@Override
	public void resume() {}
}
