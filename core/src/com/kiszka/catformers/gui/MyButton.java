package com.kiszka.catformers.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kiszka.catformers.CatformersGame;

public class MyButton {
	
	private Skin			skin;
	
	private CatformersGame			game;
	
	private TextButtonStyle	buttonDefault;
	
	public MyButton(CatformersGame game)
	{
		// load image atlas for the button states
		TextureAtlas buttonAtlas = new TextureAtlas(Gdx.files.internal("data/gui/buttons.pack"));
		
		// create a skin for the button
		Skin buttonSkin = new Skin();
		// populate the skin with the regions from the atlas
		buttonSkin.addRegions(buttonAtlas);
		
		// load the default skin file
		skin = new Skin(Gdx.files.internal("data/uiskin.json"));
		// get the default button style from the skin and fix it
		buttonDefault = skin.get("default", TextButtonStyle.class);
		// set the font to our bitmap font
		buttonDefault.font = game.largeFont;
		
		// move the button text to the center
		buttonDefault.unpressedOffsetY = 18;
		buttonDefault.pressedOffsetY = 18;
		
		// set the colors for the button and text in their different states
		buttonDefault.up = buttonSkin.getDrawable("button-main-default");
		buttonDefault.fontColor = Color.BLUE;
		buttonDefault.down = buttonSkin.getDrawable("button-main-click");
		buttonDefault.downFontColor = Color.GREEN;
		buttonDefault.over = buttonSkin.getDrawable("button-main-hover");
		buttonDefault.overFontColor = Color.RED;
		buttonDefault.disabled = buttonSkin.getDrawable("button-main-disabled");
		
	}
	
	public Table getButton(String text, ClickListener buttonListener)
	{
		// create a button and apply the modified button style
		final TextButton button = new TextButton(text, buttonDefault);
		
		// click listener
		button.addListener(buttonListener);
		
		// create a table and set it to autofill the parent element
		Table table = new Table();
		table.setFillParent(true);
		// add the button
		table.add(button).width(250).height(130);
		
		return table;
	}
	
}
