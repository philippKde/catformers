package com.kiszka.catformers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;

/**
 * MyAssetManager is an asset manager that loads and stores assets.
 * 
 * @author philipp
 * @author Eva.Kiszka
 */
public class MyAssetManager extends AssetManager {
	
	/**
	 * Constructor.
	 * 
	 */
	public MyAssetManager() {
		super();
		loadAssets();
	}
	
	/**
	 * Loads all assets the game requires.
	 */
	private void loadAssets() {
		// Sound assets
		load("sound/music/music_island.mp3", Music.class);
		load("sound/music/music_forest.mp3", Music.class);
		load("sound/music/music_desert.mp3", Music.class);
		load("sound/music/music_underwater.mp3", Music.class);
		
		load("sound/pling2.wav", Sound.class);
		load("sound/coin.wav", Sound.class);
		load("sound/jumping.wav", Sound.class);
		load("sound/teleport.wav", Sound.class);
		load("sound/health.wav", Sound.class);
		
		// Graphic assets
		/*
		 * load("data/sylvanas2_walk.png", Texture.class);
		 * load("data/sylvanas_idle.png", Texture.class);
		 * load("data/cat_powerjump2.png", Texture.class);
		 * load("data/cat6_jump_red.png", Texture.class);
		 * load("data/girljump.png", Texture.class);
		 */
		FileHandle[] files = Gdx.files.local("assets/").list();
		for (FileHandle file: files) {
			// do something interesting here
			Gdx.app.log("files", file.name());
		}
		load("data/kuerbisrun.png", Texture.class);
		load("data/background/Hills_4.png", Texture.class);
		load("data/kuerbisidle.png", Texture.class);
		load("data/kuerbisjump.png", Texture.class);
		load("data/star.png", Texture.class);
		load("data/heart.png", Texture.class);
		load("data/background/BG_forest3.png", Texture.class);
	}
	
}
