package com.kiszka.catformers;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GLTexture;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import javax.swing.text.View;

/**
 * MyGame is a cross-platform platformer.
 *
 * @author philipp
 */
public class CatformersGame extends Game {

	//private static final String	LARGE_FONT		= "data/font/buxtonsketch.fnt";
	private static final String	LARGE_FONT		= "data/font/arial16.fnt";


	private static final String	DEFAULT_FONT	= "data/default.fnt";

	private static final String	GAME_PREFS		= "gamePrefs";

	public static final int		VIEWPORT_HEIGHT	= 32;

	public static final int		VIEWPORT_WIDTH	= 32;

	String						theLevel;

	public int					screenWidth;

	public int					screenHeight;

	public ParallaxCamera		camera;

	public OrthographicCamera	paraCamera;

	public OrthographicCamera	fixedCamera;

	public SpriteBatch			batch;

	public BitmapFont			font;

	public BitmapFont			largeFont;

	public Preferences			prefs;

	public AssetManager			assets;

	public LabelStyle			labelStyle		= new LabelStyle();

	public LabelStyle			labelStyleLarge	= new LabelStyle();

	public TextFieldStyle		textFieldStyle	= new TextFieldStyle();

	public Skin					skin			= new Skin();

	private Stage				stage;

	private Label				progressLabel;
	public Viewport            viewport;

	@Override
	public void create() {
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();

		//GLTexture.setEnforcePotImages(false);
		prefs = Gdx.app.getPreferences(GAME_PREFS);

		// world and level of the player
		int playerWorld = prefs.getInteger("playerWorld");
		int playerLevel = prefs.getInteger("playerLevel");

		if(playerWorld < 1) {
            prefs.putInteger("playerWorld", 1);
        }
        if(playerLevel < 1){
		    prefs.putInteger("playerLevel", 1);
        }

		// create the cameras for our game
		createCameras();

		// create the global sprite batch for rendering
		// this is an opengl primitive
		// it is more efficient in opengl to render several sprites in a batch, then many sprites after another
		// so we add each sprite to a batch first, then draw this
		batch = new SpriteBatch();


		// create global fonts
		font = new BitmapFont(Gdx.files.internal(DEFAULT_FONT), false);
		largeFont = new BitmapFont(Gdx.files.internal(LARGE_FONT), false);

		// create label styles
		labelStyle.font = font;
		labelStyle.fontColor = Color.WHITE;

		labelStyleLarge.font = largeFont;
		labelStyleLarge.fontColor = Color.BLUE;

		assets = new MyAssetManager();

		// make the stage react to input
		Gdx.input.setInputProcessor(stage);

		// show the game splash screen
		setScreen(new SplashScreen(this, MyStage.START));
	}

	/**
	 * Creates a parallax and an orthographic camera.
	 */
	private void createCameras() {
        // camera for a 2nd bg layer
		camera = new ParallaxCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
		camera.setToOrtho(false, VIEWPORT_WIDTH, VIEWPORT_HEIGHT);

		// general camera
		fixedCamera = new OrthographicCamera();
		fixedCamera.setToOrtho(false, VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
		fixedCamera.update();


        this.viewport = new ScalingViewport(Scaling.none, screenWidth, screenHeight);
		this.viewport.setScreenSize(screenWidth, screenHeight);
        this.viewport.update(VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
        this.viewport.apply(true);

    }

	@Override
	public void render() {
		if (assets.update()) {
			super.render();
		} else {
			// Gdx.app.log("asset", "asset loaded" + assets.getProgress());
			// progressLabel.setText("" + assets.getProgress());
		}
		// get the delta time
		// float deltaTime = Gdx.graphics.getDeltaTime();
		// stage.act(deltaTime);
		//
		// stage.draw();

	}

	@Override
	public void dispose() {
		batch.dispose();
		font.dispose();
		largeFont.dispose();
		assets.dispose();
	}

	@Override
	public void resize(int width, int height) {}

	@Override
	public void pause() {}

	@Override
	public void resume() {
		setScreen(new SplashScreen(this, MyStage.START));
	}
}
